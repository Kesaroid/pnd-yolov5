import torch
import cv2
import os
from glob import glob
import pandas as pd
import json

os.environ["CUDA_VISIBLE_DEVICES"] = "1"

def pallet_level():
    img_size = (640, 512)
    batch_size = 12
    audits = os.listdir(image_dir) # ['QA2'] # 
    # Model
    model = torch.hub.load('.', 'custom', path=model_path, source='local') #.autoshape().eval()
    model.conf = 0.55  # confidence threshold (0-1)
    model.iou = 0.45  # NMS IoU threshold (0-1)
    model.classes = [0] # filter 0 for protruding nail only

    all_rows = []
    # QA1 GT
    gt = []
    gt.extend([0]*7 + [1,1] + [0]*6 + [1]*72 + [0]*8 + [1, 0])
    invalid = []

    # # QA2 GT
    # gt = []
    # gt.extend([1, 1, 0, 1, 0, 1, 0, 0] + [1]*9 + [0] + [1]*52)
    # # gt.extend([1, 0, 0, 1, 0, 0] + [1]*6 + [0] + [1]*40)
    # invalid = [2, 4, 9, 12, 16, 21, 35, 37, 40, 46, 48, 52, 53, 58, 66, 68] #, 71, 72, 73, 74]

    for qa in audits:
        folders = os.listdir(os.path.join(image_dir, qa))

        assert len(folders) > 0
        for idir in folders:
            # if idir == "20210428_002": continue # Only for QA2
            pallets = os.listdir(os.path.join(image_dir, qa, idir))

            # assert len(gt) == len(pallets)
            for idx, idx_pallet in enumerate(pallets):
                print(idx_pallet)
                if int(idx_pallet) in invalid:
                    continue
                pnd = False
                images_per_pallet = glob(os.path.join(image_dir, qa, idir, idx_pallet, '*.jpg'))
            
                for i in range(0, len(images_per_pallet), batch_size):
                    batch = []
                    for j in range(batch_size):
                        batch.append(cv2.imread(images_per_pallet[i+j])) # resizing done in model

                    # Inference
                    prediction = model(batch, size=img_size[0])
                    print(prediction.print())
                    
                    for idxr, result in enumerate(prediction.xyxy):
                        if result.nelement() == 0: continue
                        else:
                            pnd = True
                            save_inference_pallets(images_per_pallet[i+idxr], \
                                prediction.pandas().xyxy[idxr].to_json(orient="records"))
                            break
                    # if pnd: break # Use to stop inferring all images

                all_rows.append(["{}_{}_{}".format(str(qa), str(idir), str(idx_pallet)), gt[idx], 1 if pnd else 0])
                print(all_rows)
        break
    df = pd.DataFrame(all_rows, columns=['pallets', 'gt', 'pnd'])
    df.to_csv(out_file)

def save_inference_pallets(name, result):
    name_split = name.split('\\')
    pallet_dir = os.path.join(image_save_dir, name_split[-2])
    if not os.path.exists(pallet_dir):
        os.mkdir(pallet_dir)
    
    result = json.loads(result)[0]
    x_min, y_min, x_max, y_max = int(result['xmin']), int(result['ymin']), int(result['xmax']), int(result['ymax'])
    
    image = cv2.imread(name)
    cv2.rectangle(image, (x_min, y_min), (x_max, y_max), (0,255,0), 2)
    cv2.putText(image, 'pnc: {}'.format(result['confidence']), (x_min, y_min-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (36,255,12), 2)
    
    cv2.imwrite(os.path.join(pallet_dir, name_split[-1]), image)


def main():
    img_size = (640, 512) # (1280, 1024) # 
    batch_size = 1
    audits = os.listdir(image_dir)
    # Model
    model = torch.hub.load('.', 'custom', path=model_path, source='local') #.autoshape().eval()
    model.conf = 0.55  # confidence threshold (0-1)
    model.iou = 0.45  # NMS IoU threshold (0-1)
    # model.classes = [0, 5, 6] # filter 0 for protruding nail only

    for qa in audits:
        folders = os.listdir(os.path.join(image_dir, qa))

        assert len(folders) > 0
        for idir in folders:
            pallets = os.listdir(os.path.join(image_dir, qa, idir))
            # assert len(gt) == len(pallets)
            for idx, idx_pallet in enumerate(pallets):
                print(idx_pallet)
                pnd = False
                images_per_pallet = glob(os.path.join(image_dir, qa, idir, idx_pallet, '*.jpg'))
            
                for i in range(0, len(images_per_pallet), batch_size):
                    batch = []
                    for j in range(batch_size):
                        batch.append(cv2.imread(images_per_pallet[i+j])) # resizing done in model
                        # batch.append(cv2.resize(cv2.imread(images_per_pallet[i+j]), img_size))
                        # batch.append(cv2.cvtColor(cv2.imread(images_per_pallet[i+j]), cv2.COLOR_BGR2RGB)) # resizing done in model

                    # Inference
                    prediction = model(batch, size=img_size[0])
                    print(prediction.print())
                    
                    for idxr, result in enumerate(prediction.xyxy):
                        if result.nelement() == 0: continue
                        else:
                            pnd = True
                            save_inference(images_per_pallet[i+idxr], \
                                prediction.pandas().xyxy[idxr].to_json(orient="records"), img_size)
                    # if pnd: break # Use to stop inferring all images


def save_inference(name, result, img_size):
    name_split = name.split('\\')
    # pallet_dir = os.path.join(image_save_dir, name_split[-2])
    pallet_dir = os.path.join(image_save_dir, name_split[-4], name_split[-3], name_split[-2])
    if not os.path.exists(pallet_dir):
        os.makedirs(pallet_dir)
    
    image = cv2.imread(name)
    # image = cv2.resize(cv2.imread(name), img_size)
    results = json.loads(result)
    if len(results) > 1: print(results)
    for result in results:
        x_min, y_min, x_max, y_max = int(result['xmin']), int(result['ymin']), int(result['xmax']), int(result['ymax'])
        
        cv2.rectangle(image, (x_min, y_min), (x_max, y_max), (0,255,0), 2)
        cv2.putText(image, '{}: {}'.format(result['name'], result['confidence']), (x_min, y_min-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (36,255,12), 2)
    
    cv2.imwrite(os.path.join(pallet_dir, name_split[-1]), image)


if __name__ == "__main__":
    # model_path = "runs/train/v5x epoch 2+5/weights/best.pt"
    model_path = "runs/train/v5s_resized/weights/best.pt"
    image_dir = "H:/PND/valid_data/audits"
    image_save_dir = "pallet_level/threshold=0.55"
    out_file = "pallet_level/threshold=0.55/qa1-0.55.csv"
    # main()
    pallet_level()